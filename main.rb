#encoding: UTF-8
require "./Bibliotecas/env"


#########################################################################
# Dias de corte da data do chamado para atuacao do bot
dias_corte = 40
# Data atual menos 1 mes
datacorte = Date.today - dias_corte

$logger.info "Data Corte: #{datacorte}"
#########################################################################
begin
# ---------------------------
# ----- Inicio processo -----
# ---------------------------
$logger.info 'Inicio processo'

$logger.info 'Abrir Tate'

# Acessar pagina Tate
$browser.goto $url_tate

# Realizar Login Tate
$weblogin_tate.login_tate($user_tate,$password_tate)

# Clicar em Incident>Assign to me
$webhomepage_tate.menu_assigned_to_me.click

sleep 5

# Ler chamados Tate "Assigned to me"
$logger.info 'Chamados Tate assigned to me'
chamadosTate = $webchamadoassigned_tate.ler_chamados_page
# binding.pry

puts "Chamados Tate WEB => #{chamadosTate}"

#$arrayteste = ['INC0292236','INC0196745','INC0155546','INC0210333','INC1234567','INC9876543']

# $logger.info 'Buscar chamados Tate na API SpreadUP'
# $apispreadup.verificar2'INC0292236'

chamadosTate.each {|item|
  puts item
  chamado_api = $apispreadup.verificar_chamado(item)
  $logger.info "chamado_api: #{chamado_api}"
  #puts $apispreadup.verificar_chamado.body
  if chamado_api == ""
    $logger.info "Novo chamado #{item}"
    $logger.info 'Buscar chamados Tate no filtro de pesquisa'
    # Filtro combobox selecionar number
    $webchamadoassigned_tate.filtro_list.select 'number'
    # Buscar 1° chamado 
    $webchamadoassigned_tate.filtro_pesquisa.set item
    sleep 1   
    $webchamadoassigned_tate.sendkeys_enter
    sleep 1
    # Capturar data de abertura do chamado
    dataopen =  Date.parse $webchamadoassigned_tate.date_open
    $logger.info "Data de abertura do chamado #{dataopen}"
    # Validar se data de abertura do chamado e maior que 30 dias
    if dataopen >= datacorte 
      # Clicar no chamado
      $webchamadoassigned_tate.click_chamado.click
      sleep 2
      # Capturar informações do chamado 
      $logger.info 'Capturar dados chamado -Tate'
      # Capturar incidente
      $vcod_incident = $webchamado_tate.incidente_value 
      $logger.info "Incidente = >#{$vcod_incident}"
      # Capturar short_description
      $vdescricaoresumida = $webchamado_tate.short_description_value
      $logger.info "Short Description = >#{$vdescricaoresumida}"
      # Capturar description
      $vdescription = $webchamado_tate.description_value
      $logger.info "Description = >#{$vdescription}"
      # Clicar em Incident>Assign to me
      $webhomepage_tate.menu_assigned_to_me.click
      sleep 2
      $logger.info "Criar chamado no Spreadup, codigo Tate #{$vcod_incident}"
      # Criar chamado SpreaUP
      $apispreadup.criar_chamado
      $logger.info "Chamado inserido no Spreadup, codigo Tate #{$vcod_incident}"
      puts '##########################################################################'    
    else
      $logger.info "Chamado #{item} e maior que #{dias_corte} dias"
      puts '##########################################################################'
      next
    end

  else 
    $logger.info "Chamado #{item} ja existe, codigo SpreadUP #{chamado_api}"      
  end
  puts '##########################################################################'
}
$logger.info 'Fim da execucao do BOT'
puts '##########################################################################'

rescue StandardError => $e
   puts "Ocorreu um erro no processo #{$e.inspect}"
   raise "#{$e.inspect}" # Raise estoura erro e e falha no build
   # Disparo de email
   $email.mail_process_error
end  