require './Bibliotecas/env'

# $username = 'SAP'
# $password = 'spread123'
# $customerid = 'ECC9A46D-6327-47E2-8A0C-8C30351AF517'
# $clientnumber = 'SUZANOR0001'

class API_Spreadup

    def criar_chamado
        begin
        response = HTTParty.post($post,
            :basic_auth => {'username': $username_api, 'password': $password_api},
            :headers => {'cache-control': 'no-cache', 'content-type': 'application/json'},
            :timeout => 180,
            :body => {"Details": $vdescription,
                        "Description": "#{$vdescricaoresumida} - #{$vcod_incident}",
                        "ClientNumber": $vcod_incident,
                        "CustomerId": $customer_id,
                        "EngagementId": $engagement_id,
                        "ProductId": $product_id}.to_json)

                        if response.code == 200
                            puts "OK",  response.code, response.body
                        else
                            puts 'Ocorreu um erro na API SpreadUP cadastrar chamado', response.code, response.body
                            $email.mail_api_spreadup_error
                        end
                        return response
        rescue StandardError => $e
            puts "Ocorreu um erro na API SpreadUP Cadastro do chamado #{$e.inspect}"
            puts response.code, response.body
        #puts response.code, response.body
        end
    end

    def verificar_chamado(nr_chamado)
        begin
            result_api = Array.new          
            get_chamado = HTTParty.get($get,
                :basic_auth => {'username': $username_api, 'password': $password_api},        
                :headers => {'cache-control': 'no-cache','content-type': 'application/json'},
                :query => {:customerid=> $customer_id, :clientnumber=> nr_chamado})

                if get_chamado.code == 200
                    # puts "OK",  get_chamado.code, get_chamado.body
                else
                    puts "Ocorreu um erro na API SpreadUP verificar chamado. #{get_chamado.code} #{get_chamado.body}"
                    $email.mail_api_spreadup_error
                end

            return get_chamado['number']
        rescue StandardError => $e
            puts "Ocorreu um erro na API SpreadUP verificar chamado #{$e.inspect}"
            raise "#{$e.inspect}" # Raise estoura erro e e falha no build
            $email.mail_api_spreadup_error
            #puts get_chamado.code, get_chamado.body
        end
    end

    def verificar2(numero_chamado)
        get_chamado = HTTParty.get('http://192.168.229.17:8099/api/sap/existrequest',
            :basic_auth => {'username': 'SAP', 'password': 'spread123'},        
            :headers => {'cache-control': 'no-cache','content-type': 'application/json'},
            :query => {:customerid=> '09BAF348-9802-4EC8-86C7-9B917A9BEBFF', :clientnumber=> numero_chamado})

            puts get_chamado.code, get_chamado.body
            return get_chamado['number']
    end
end