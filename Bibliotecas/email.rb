#encoding: UTF-8

require './Bibliotecas/env'

# rpa.spread@gmail.com (spread@123)

#$lista_email = ['Receiver Robo <rpa.spread@gmail.com>', 'Receiver Jose <zezefischersevero@gmail.com>']
$lista_email = 'Receiver Jose <zezefischersevero@gmail.com>' #suporte-fabrica@spread.com.br
$lista_email_cc = 'Receiver Severo <jose.severo@spread.com.br>'
$mailer = EmailSender.new_gmail(from: 'rpa.spread@gmail.com', password: 'spread@123')
# mailer.send(to: [lista_email],cc: ["Jose2 <jose.severo@spread.com.br>"],

class Email
  def mail_login_tate_error
    $mailer.send(to: $lista_email,
            cc: $lista_email_cc,
            subject: 'Falha Login Tate',
            content: "Falha no Login usuario= #{$user_tate}, senha= #{$password_tate}.")
  end

  def mail_api_spreadup_error
    $mailer.send(to: $lista_email,
            cc: $lista_email_cc,
            subject: 'Falha na API SpreadUp',
            content: "Falha na API SpreadUp.")
  end


  def mail_process_error
    $mailer.send(to: $lista_email,
          cc: $lista_email_cc,
          subject: 'Erro no processo Tate',
          content: "Ocorreu um erro no processo Tate.\n#{$e.inspect}")        
  end
end

