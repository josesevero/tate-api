#encoding: UTF-8
################################################################################
# SETUP
################################################################################
# Blibliotecas de execução
require 'chronic'
require 'date'
require 'email_sender'
require 'fileutils'
require 'find'
require 'httparty'
require 'logging'
require 'net/smtp'
require 'tiny_tds'
require 'win32ole'
require 'yaml'
require 'watir'
require 'pry'

# Importação das bibliotecas
# Dir["./Bibliotecas/*.rb"].each {|file| require file}

require './Bibliotecas/email'
require './Bibliotecas/ConfiguracaoAcessos'
require './Bibliotecas/api_spreadup'

# Instância email
$email = Email.new

# Importacao das paginas e arquivos de parametros WEB
require './Bibliotecas/pages/weblogin_tate'
require './Bibliotecas/pages/webhomepage_tate'
require './Bibliotecas/pages/webchamadoassignedto_tate'
require './Bibliotecas/pages/webchamado_tate'

# Instancia dos objetos da PageObjects WEB
$weblogin_tate = WebLogin.new
$webhomepage_tate = WebHomePage.new
$webchamadoassigned_tate = WebChamadoAssigned.new
$webchamado_tate = WebChamado.new


# Instancia dos objetos da API Spreadup
$apispreadup = API_Spreadup.new


# Browser config Tate
$browser = Watir::Browser.new :chrome#, headless: true 
$browser.window.maximize


# Logs
$logger = Logging.logger(STDOUT)
$logger.level = :debug

# Configuracao de acessos
$url_tate = ConfiguracaoAcessos['url_tate']
$user_tate = ConfiguracaoAcessos['user_tate']
$password_tate = ConfiguracaoAcessos['password_tate']
$post = ConfiguracaoAcessos['post']
$get = ConfiguracaoAcessos['get']
$username_api = ConfiguracaoAcessos['username_api']
$password_api = ConfiguracaoAcessos['password_api']
$customer_id = ConfiguracaoAcessos['cliente_cod']
$engagement_id = ConfiguracaoAcessos['engagement_id']
$product_id = ConfiguracaoAcessos['product_id']
