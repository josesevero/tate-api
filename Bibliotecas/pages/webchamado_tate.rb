#encoding: UTF-8

require "./Bibliotecas/env"

class WebChamado

    def incidente
        $browser.iframe(id: 'gsft_main').input(id: 'sys_readonly.incident.number')
    end

    def incidente_value
        return incidente.value.to_s
    end

    def short_description
        $browser.iframe(id: 'gsft_main').input(id: 'incident.short_description')
    end

    def short_description_value
        return short_description.value.to_s
    end
    
    def description
        $browser.iframe(id: 'gsft_main').textarea(id: 'incident.description')
    end

    def description_value
        return description.value.to_s
    end

end