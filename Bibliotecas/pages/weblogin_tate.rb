#encoding: UTF-8

require "./Bibliotecas/env"

class WebLogin

    def usuario_tate
        $browser.window(url: '/login/').use
        $browser.text_field(id: "username").wait_until(&:present?)
    end

    def senha_tate
        $browser.text_field(id: "password")
    end

    def bt_login
        $browser.button(type: "submit")
    end

    def msg_falha_login
        $browser.div(class: "alert__message")
    end
    def banner_home
        $browser.image(id: "mainBannerImage16") 
    end

    def login_tate(usuario, senha)
        begin
            usuario_tate.set usuario
            senha_tate.set senha
            bt_login.click
            sleep 5
            if msg_falha_login.exist? == true 
                $email.mail_login_error
                puts "Falha no login Tate"
            end                
            banner_home.wait_until(&:exist?)
            puts "Login Tate com sucesso!"
        rescue StandardError => $e
            puts "Ocorreu um erro no login Tate #{$e.inspect}"
            # Disparo de erro no email
            $email.mail_login_tate_error
            raise "#{$e.inspect}"
        end
        
    end

end