#encoding: UTF-8

require "./Bibliotecas/env"

class WebChamadoAssigned

    def ler_chamados_page
        begin
            $browser.window(title: 'Incidents | IT Service Desk').use            
            $chamados = $browser.iframe(id: 'gsft_main').elements(css: "tr[record_class='incident']>td[class='vt']>a[class='linked formlink']")
            array_chamados = Array.new
            $chamados.map do |chamado|
            array_chamados.push(chamado.text)
            end
        return array_chamados
        rescue StandardError => $e
            puts "Ocorreu um erro na leitura de chamados #{$e.inspect}"
            raise "#{$e.inspect}" # Raise estoura erro e e falha no build
            # Disparo de email
            $email.mail_process_error
        end 

        
    end

    def filtro_list
        $browser.iframe(id: 'gsft_main').select_list(class: 'form-control')
    end

    def filtro_pesquisa
        $browser.window(title: 'Incidents | IT Service Desk').use
        $browser.iframe(id: 'gsft_main').text_field(class: 'form-control')
    end

    def sendkeys_enter
        $browser.send_keys :enter
    end

    def click_chamado
        $browser.iframe(id: 'gsft_main').td(class: 'vt')
    end

    def date_open
        $browser.iframe(id: 'gsft_main').tr(class: 'list_row').td(index: 14).text
    end

end