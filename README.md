# RPA Tate

**Cliente**: Interno
**Projeto**: Tate & Lyle
**Fornecedor**: Spread Tecnologia

# Introdução
Projeto de RPA para ser executar os processos do sistema web "Tate" e SpreadUP.

# Objetivo
O robô Tate tem como objetivo de realizar a abertura dos chamados na aplicação Spread up baseado nas informações contidas nos chamados abertos na aplicação Service Now (do cliente Tate & Lyle).
O robô aplica um filtro para atender chamados no Tate com data de criação menor que 40 dias(parametrizável) da data atual.

# Configuração
**Pré-condições**
- Ruby 2.5.5
- Ruby DevKit-mingw64-32-4.7.2-20130224-1151-sfx
- Navegador Chrome Versão 78.0.3904.70 (Versão oficial) 64 bits 
- chromedriver.exe 
- Gem "bundler" do ruby

**Instalação das gemas do ruby**
No prompt de comando, digitar o comando abaixo dentro do diretório raiz do projeto clonado.
- Instalação do DEVKIT:
na pasta C: na linha de comando digitar ruby dk.rb init e depois ruby dk.rb install
- Instalaçao das Gem's
````Bundler
bundle install
````

**Gemas do ruby (Gemfile) a serem instaladas**
````Gemfile 
source 'http://rubygems.org'

gem 'bundler', '~> 2.1', '>= 2.1.1'
gem 'chronic', '~> 0.10.2'
gem 'email_sender', '~> 1.2'
gem 'httparty', '~> 0.17.3'
gem 'logging', '~> 2.2', '>= 2.2.2'
gem 'mail', '~> 2.7', '>= 2.7.1'
gem 'pry', '~> 0.12.2' 
gem 'rubysl-yaml', '2.1.0'
gem 'rubyzip', '1.3.0'
gem 'selenium-webdriver', '~> 3.142', '>= 3.142.6'
gem 'tiny_tds', '~> 2.1', '>= 2.1.2'
gem 'watir', '~> 6.16', '>= 6.16.5'
gem 'webdrivers', '~> 3.0'
gem 'win32-api', '1.6.1.2'


# Interações
 - Ambiente web Tate 
 - API SpreadUP
 - Disparo de email

**Instalação dos drivers de navegador**
Baixar e copiar o arquivo chromedriver.exe dentro do subdiretório "/bin" da pasta de instalação do ruby.
Exemplo: *'C:\Ruby23\bin'*

***Versões utilizadas durante o desenvolvimento***
- *Sistema operacional: Windows 10 Pro 64 Bits*
- *Ruby 2.5.5p157 
- *Ruby DevKit-mingw64-32-4.7.2-20130224-1151-sfx.exe*
- *Navegador Chrome utilizado a Versão 78.0.3904.70 (Versão oficial) 64 bits
- *Chrome version 78*
    link: https://chromedriver.storage.googleapis.com/index.html?path=79.0.3945.36/

***Hardware utilizado durante o desenvolvimento***
- *Processador: Intel Core i5 CPU 2.70GHz
- *Memória RAM: 8GB
- *Unidade de disco: HD 500GB

Código no repósitório:
https://bitbucket.org/josesevero/tate-api/src/master/

VM Tate 
- Servidor: 10.10.99.82

Jenkins
A VM está configurada para executar o arquivo "jenkins.bat" ao iniciar, caso ele nãoo inicialize, executar os comandos abaixo:
- Acessar o terminal (Cmder) e acessar o caminho abaixo para acessar o diretório C:
cd\
- Digitar o comando abaixo:
 java -jar jenkins.war
- Acessar  http://localhost:8080/login (na VM)

            http://10.10.99.82:8080/login?from=%2F  (fora da VM)

* BOT está configurado para executar a cada 1h, sendo início das 9h às 18h (segunda a sexta)
